# Building that site

1. Clone the `site-gen` branch of OctoBot
```git clone -b site-gen https://gitlab.com/aigis_bot/octobot --recursive```
2. Install requirements
```python3 -m pip install requirements.txt```
3. Run the doc_gen.py. Resulting site will be in website/result directory
```python3 doc_gen.py```